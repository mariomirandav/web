var createError = require('http-errors');
var express = require('express');
var session = require('express-session');
var path = require('path');
var cookieParser = require('cookie-parser');
var lessMiddleware = require('less-middleware');
var logger = require('morgan');

// var usersRouter = require('./routes/users');
var indexRouter           = require('./routes/index');
var herramientasRouter    = require('./routes/herramientas');
var usuariosRouter        = require('./routes/usuarios');
var epcsRouter            = require('./routes/epcs');
var herramientaTipoRouter = require('./routes/herramientatipo');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json({limit:'50mb'}));
app.use(session({secret: 'tjoLeM4H1K__6zVBRvmrup'}));
app.use(express.urlencoded({ extended: false, limit: '20mb' }));
app.use(cookieParser());
app.use(lessMiddleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/herramientas', herramientasRouter);
app.use('/usuarios', usuariosRouter);
app.use('/epcs', epcsRouter);
app.use('/herramientatipo', herramientaTipoRouter);

app.locals.moment = require('moment');

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
