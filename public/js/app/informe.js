

$(function(){

    $('#datetimepicker9').datetimepicker({
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
        format: 'DD/MM/YYYY'
    });

    $('#datetimepicker10').datetimepicker({
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
        format: 'DD/MM/YYYY'
    });
})



var $solicitar = document.querySelector('.solicitar');
var $btn_descarga = document.querySelector('.descarga');


$btn_descarga.addEventListener('click', function(e){
    e.preventDefault();
    location.href= '/download/report/report.xlsx';
})


$solicitar.addEventListener('click', function(e) {
    e.preventDefault();

    var fecha_inicio  = document.querySelector('input[name=fechaInicio]').value;
    var fecha_termino = document.querySelector('input[name=fechaTermino]').value;
    var tipo          = document.querySelector('select[name=idTipo]').value;

    var xhr = new XMLHttpRequest();

    var form_data = new FormData();
    form_data.append('date_start', fecha_inicio);
    form_data.append('date_end', fecha_termino);
    form_data.append('type', tipo);

    var send = {
        date_start: fecha_inicio,
        date_end: fecha_termino,
        type: tipo
    }

    var jsonString = JSON.stringify( send );

    
    xhr.open('POST','/ajax/reporte', true);


    xhr.onload = function(){
        if( xhr.readyState === 4 ) {
            if( xhr.status === 200 ) {
                var data = JSON.parse(xhr.responseText);
                if ( data.success ) {
                    
                    var $btn = document.querySelector('.descarga');
                    $btn.removeAttribute('disabled');

                }
            }
        }
    }

    xhr.setRequestHeader('Content-Type', 'application/json');

    //xhr.send( form_data );
    xhr.send( jsonString );

});


obtiene_tipo_herramientas();

function obtiene_tipo_herramientas(){
    var xhr = new XMLHttpRequest();
    
    xhr.open('POST','/herramientas/ajax/tipo_herramientas', true);
    
    xhr.onload = function(data) {
        if( xhr.readyState === 4 ) {
            if( xhr.status === 200 ) {
                var data = JSON.parse(xhr.responseText);
                var $select = document.querySelector('select[name=idTipo]');
                
                if( data.success ) {
                    
                    var nodos = data.response.map(function(item) {
                        return '<option value="' + item.id + '">' + item.tipo + '</option>';
                    });

                    nodos.unshift('<option value="-1">Todos los tipos de herramientas</option>');

                    $select.innerHTML = nodos.join('');

                }
            }
        }
    }

    xhr.send();
}