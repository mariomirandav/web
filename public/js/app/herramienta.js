// https://stackoverflow.com/questions/36280818/how-to-convert-file-to-base64-in-javascript
function getFiles(files) {
    return Promise.all(files.map(file => getFile(file)));
}

//take a single JavaScript File object
function getFile(file) {
    var reader = new FileReader();
    return new Promise((resolve, reject) => {
        reader.onerror = () => { reader.abort(); reject(new Error("Error parsing file"));}
        reader.onload = function () {

            //This will result in an array that will be recognized by C#.NET WebApi as a byte[]
            let bytes = Array.from(new Uint8Array(this.result));

            //if you want the base64encoded file you would use the below line:
            let base64StringFile = btoa(bytes.map((item) => String.fromCharCode(item)).join(""));

            //Resolve the promise with your custom file structure
            resolve({ 
                bytes: bytes,
                base64StringFile: base64StringFile,
                fileName: file.name, 
                fileType: file.type
            });
        }
        reader.readAsArrayBuffer(file);
    });
}

$(function(){

    $('#datetimepicker9').datetimepicker({
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
        format: 'DD/MM/YYYY',
        DefaultDate: false
    });

    $('#datetimepicker9').val('')
})

//resolver imagen, retornara la imagen en un json definido en getFile:

var $imagen = document.querySelector('input[type=file]');

$imagen.addEventListener('change', (event) => {
    let file = document.querySelector('input[type="file"]').files[0]
    let $show_image = document.querySelector('#image_t');
    
    getFile( file ).then((customJsonFile) => {
        //console.log( customJsonFile )
        let $file_hidden = document.querySelector('input[name=full_image]');

        if( customJsonFile.fileType != 'image/jpeg' ) {
            console.error('imagen invalida');
            $file_hidden.value = '';
            $show_image.src = '';
            return;
        }

        $file_hidden.value = 'data:' + customJsonFile.fileType + ';base64,' + customJsonFile.base64StringFile;
        
        $show_image.src = 'data:' + customJsonFile.fileType + ';base64,' + customJsonFile.base64StringFile;

    })
    
})



let $btn_submit = document.querySelector('form');

$btn_submit.addEventListener('submit', (event) => {
    
    if( ! valida_formulario() ){ 
        event.preventDefault();
        return;
    }


    return true;

});


var $select_epc = document.querySelector('select[name=epc_list]');

if( $select_epc != null ) {
    obtiene_epcs_disponibles();

    $select_epc.addEventListener('change', function(e) {
        var valor = this.value;
        
        var $epc_field = document.querySelector('input[name=epc]');
        $epc_field.value = valor;
    });

}

obtiene_tipo_herramientas();

function obtiene_tipo_herramientas(){
    var xhr = new XMLHttpRequest();
    xhr.open('POST','/herramientas/ajax/tipo_herramientas', true);
    xhr.onload = function(data) {
        if( xhr.readyState === 4 ) {
            if( xhr.status === 200 ) {
                var data = JSON.parse(xhr.responseText);
                var $select = document.querySelector('select[name=idTipo]');
                
                if( data.success ) {
                    
                    var nodos = data.response.map(function(item) {
                        return '<option value="' + item.id + '">' + item.tipo + '</option>';
                    });

                    nodos.unshift('<option value="">Selecciona un tipo de herramienta</option>');

                    $select.innerHTML = nodos.join('');

                    var tipo_seleccionado = document.querySelector('input[name=_tipo_hidden]');
                    
                    var $select_full = document.querySelectorAll('select[name=idTipo] option');

                    $select_full.forEach(function(item) {

                        if( tipo_seleccionado.value == item.value ) {
                            item.setAttribute('selected', 'selected');
                        }

                    });

                }
            }
        }
    }


    xhr.send();
}

function obtiene_epcs_disponibles() {
    var xhr = new XMLHttpRequest();
    xhr.open('POST','/herramientas/ajax/epc_available', true);
    xhr.onload = function(data) {
        if( xhr.readyState === 4 ) {
            if( xhr.status === 200 ) {
                var data = JSON.parse(xhr.responseText);
                var $select = document.querySelector('select[name=epc_list]');
                
                if( data.success ) {
                    
                    var nodos = data.response.map(function(item) {
                        return '<option value="' + item.epc + '">' + item.idMark + ' / ' +item.epc + '</option>';
                    });

                    nodos.unshift('<option value="">Selecciona un epc disponible</option>');

                    $select.innerHTML = nodos.join('');
                }
            }
        }
    }


    xhr.send();

}

function valida_formulario(){
    let $full_img = document.querySelector('input[name=full_image]');
    let $nombre   = document.querySelector('input[name=nombre]');
    let $epc      = document.querySelector('input[name=epc]');
    let $detalle  = document.querySelector('textarea[name=detalle]');
    let $idTipo   = document.querySelector('select[name=idTipo]');

    let valido = true;

    
    document.querySelector('input[name=imagen]').classList.remove('error');
    $nombre.classList.remove('error');
    $epc.classList.remove('error');
    $detalle.classList.remove('error');
    $idTipo.classList.remove('error');


    if( $full_img.value == '' )    {
        $full_img.classList.add('error');
        valido = false;
    }
    if( $nombre.value == '' )    {
        $nombre.classList.add('error');
        valido = false;
    }
    if( $epc.value == '' ){
        $epc.classList.add('error');
        valido = false;
    }
    if( $detalle.value == '' ){
        $detalle.classList.add('error');
        valido = false;
    }
    if( $idTipo.value <= 0 ){
        $idTipo.classList.add('error');
        valido = false;
    }
        
    if( $full_img.value == '' ) {
        document.querySelector('input[name=imagen]').classList.add('error');
        valido = false;
    }
    

    return valido;

}