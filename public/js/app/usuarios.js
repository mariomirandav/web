// https://stackoverflow.com/questions/36280818/how-to-convert-file-to-base64-in-javascript
function getFiles(files) {
    return Promise.all(files.map(file => getFile(file)));
}

//take a single JavaScript File object
function getFile(file) {
    var reader = new FileReader();
    return new Promise((resolve, reject) => {
        reader.onerror = () => { reader.abort(); reject(new Error("Error parsing file"));}
        reader.onload = function () {

            //This will result in an array that will be recognized by C#.NET WebApi as a byte[]
            let bytes = Array.from(new Uint8Array(this.result));

            //if you want the base64encoded file you would use the below line:
            let base64StringFile = btoa(bytes.map((item) => String.fromCharCode(item)).join(""));

            //Resolve the promise with your custom file structure
            resolve({ 
                bytes: bytes,
                base64StringFile: base64StringFile,
                fileName: file.name, 
                fileType: file.type
            });
        }
        reader.readAsArrayBuffer(file);
    });
}

//resolver imagen, retornara la imagen en un json definido en getFile:

var $imagen = document.querySelector('input[type=file]');

$imagen.addEventListener('change', (event) => {
    let file = document.querySelector('input[type="file"]').files[0]
    let $show_image = document.querySelector('#image_t');
    
    getFile( file ).then((customJsonFile) => {
        //console.log( customJsonFile )
        let $file_hidden = document.querySelector('input[name=full_image]');

        if( customJsonFile.fileType != 'image/jpeg' ) {
            console.error('imagen invalida');
            $file_hidden.value = '';
            $show_image.src = '';
            return;
        }

        $file_hidden.value = 'data:' + customJsonFile.fileType + ';base64,' + customJsonFile.base64StringFile;
        
        $show_image.src = 'data:' + customJsonFile.fileType + ';base64,' + customJsonFile.base64StringFile;

    })
    
})


let $btn_submit = document.querySelector('form');

$btn_submit.addEventListener('submit', (event) => {
    //event.preventDefault();
    
    if( ! valida_formulario() ){ 
        event.preventDefault();
        return;
    }

    
    return true;
});


function valida_formulario(){
    let $nombre               = document.querySelector('input[name=nombre]');
    let $tipo                 = document.querySelector('select[name=tipo]');
    let $identificador_lector = document.querySelector('input[name=lector_identificador]');
    let $full_img             = document.querySelector('input[name=full_image]');

    let valido = true;

    $nombre.classList.remove('error');
    $tipo.classList.remove('error');
    $identificador_lector.classList.remove('error');
    $full_img.classList.remove('error');


    if( $nombre.value == '' ) {
        $nombre.classList.add('error');
        valido = false;
    }
    
    if( $tipo.value == '' ) {
        $tipo.classList.add('error');
        valido = false;
    }
    
    if( $identificador_lector.value == '' ) {
        $identificador_lector.classList.add('error');
        valido = false;
    }
    
    if( $tipo.value <= 0 ) {
        $tipo.classList.add('error');
        valido = false;
    }

    if( $full_img.value == '' ) {
        document.querySelector('input[name=imagen]').classList.add('error');
        valido = false;
    }

    return valido;

}