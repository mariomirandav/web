var dashboard = {

    template: () => {
        let html = `<div id="{{ IDTIPO }}" class="col-md-4" style="margin-bottom:30px;margin-top:20px">
                        <div class="image-container" style="position:absolute;">
                            <div class="img-content">
                                <img src="{{ IMAGE }}" width="75px" height="75px">
                            </div>
                        </div>
                        <div class="card custom-card text-center">
                        <div class="container-fluid more-margin">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="titulo">{{ NOMBRE }}</p>
                                    
                                </div>
                            <div class="col-md-12">
                                <span class="titulo_disponible">Disponibles</span>
                            </div>
                            <div class="col-md-12">
                                <p class="disponible badge-success">
                                    <span style="">{{ DISPONIBLES }}</span><br/>
                                    <span style="">{{ PUERTAS }}</span>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="titulo_secundario">No disponibles</p>
                                <div class="text-center">
                                    <p class="badge badge-warning numero">{{ NO_DISPONIBLE }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <p class="titulo_secundario">Inutilizables</p>
                            <div class="text-center">
                                <p class="badge badge-danger numero">{{ INUTILIZABLE }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`;

        return html;
    },


    load_interval: () => {

        setInterval( () => {
            dashboard.load_dashboard();
        },10000 )

    },

    load_dashboard: () => {
        let req = new XMLHttpRequest();

        

        req.open('POST', '/ajax/dashboard', true);

        req.onload = ( data ) => {
            if( req.readyState === 4 ) {
                if( req.status === 200 ) {
                    let json = JSON.parse( req.responseText );
                    

                    if( ! json.status ) {
                        console.log('datos no encontrados')
                        let $dashboard_content = document.querySelector('.contenido_dashboard');

                        $dashboard_content.innerHTML = '<h2>No se han encontrado resultados</h2>';
                        return;
                    }
                    
                    let html_nodos = [];
                    json.result.inventario.forEach(element => {
                        
                        let tpl = dashboard.template();

                        tpl = tpl.split('{{ IMAGE }}').join(element.img);
                        tpl = tpl.split('{{ NOMBRE }}').join(element.nombre);
                        tpl = tpl.split('{{ DISPONIBLES }}').join(element.disponible);
                        tpl = tpl.split('{{ NO_DISPONIBLE }}').join(element.nodisponible);
                        tpl = tpl.split('{{ INUTILIZABLE }}').join(element.inutilizables);
                        tpl = tpl.split('{{ IDTIPO }}').join(element.idTipo);
                        tpl = tpl.split('{{ PUERTAS }}').join(element.puerta_disponibles);

                        html_nodos.push( tpl );

                    });



                    let $dashboard_content = document.querySelector('.contenido_dashboard');

                    $dashboard_content.innerHTML = html_nodos.join('');

                    let $numero_disponible = document.querySelector('.card-disponible .numero_card');
                    $numero_disponible.innerHTML = json.result.disponible[0].count;

                    let $numero_no_disponible = document.querySelector('.card-no-disponible .numero_card');
                    $numero_no_disponible.innerHTML = json.result.usado[0].count;

                    let $numero_inutilizados = document.querySelector('.card-inutilizable .numero_card');
                    $numero_inutilizados.innerHTML = json.result.inutilizados[0].count;



                } else {
                    let $dashboard_content = document.querySelector('.contenido_dashboard');

                    $dashboard_content.innerHTML = '<h2>No se han encontrado resultados</h2>';
                }
            }
        }

        req.send();

    },

    init: () => {
        dashboard.load_dashboard();
        dashboard.load_interval();
    }
 
}


dashboard.init();