var $form = document.querySelector('#form');

$form.addEventListener('submit', function(e){

    var $idMark = document.querySelector('input[name=idMark]');
    var $epc    = document.querySelector('input[name=epc]');
    var valido  = true;

    $idMark.classList.remove('error');
    $epc.classList.remove('error');


    if( $epc.value == '' ) {
        valido = false;
        $epc.classList.add('error');
    }

    if( $idMark.value == '' ) {
        valido = false;
        $idMark.classList.add('error');
    }


    if( ! valido ) {
        e.preventDefault();
        return;
    }


});
