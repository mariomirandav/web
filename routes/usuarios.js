var express = require('express');
var router = express.Router();
var fetch = require('node-fetch');
var session = require('express-session');
var go_config = require('../bin/config');

/* GET home page. */
router.get('/', function(req, res, next) {
    
    var endpoint = go_config.endpoint + '/usuarios/get_all';

    var mensaje_error = req.session.error_msg;
    var mensaje_success = req.session.success_msg;

    req.session.success_msg = null;
    req.session.error_msg = null;

    fetch( endpoint,{
        method: 'POST',
        headers: go_config.fetch_headers_default,
    } )
        .then( res => res.json() )
        .then( json => {
            
            if( json.success ) {
                res.render('backend/usuarios/index', { 
                    data        : json.data,
                    msg_success : mensaje_success,
                    msg_error   : mensaje_error
                });
            } else {
                res.render('backend/usuarios/error');
            }

        } )
        .catch( err => {
            res.render('backend/usuarios/error');
        } )

});

router.get('/edit/:id', function( req, res, next ) {
    var endpoint = go_config.endpoint + '/usuarios/get_by_id';
    var id = req.params['id'];
    var mensaje_error = req.session.error_msg;
    var mensaje_success = req.session.success_msg;

    req.session.success_msg = null;
    req.session.error_msg = null;

    fetch( endpoint, {
        method: 'POST',
        headers: go_config.fetch_headers_default,
        body: JSON.stringify({id: id}),
    } )
        .then( res => res.json() )
        .then( json => {
            res.render('backend/usuarios/form', {
                id                   : json.data.id,
                nombre               : json.data.nombre,
                img                  : json.data.img,
                tipo                 : json.data.tipo,
                lector_identificador : json.data.lector_identificador,
                msg_success          : mensaje_success,
                msg_error            : mensaje_error
            })
        }).catch((err) => {
            res.render('backend/herramientas/error')
        });


});

router.get('/create', function( req, res, next ){

    var mensaje_error = req.session.error_msg;
    var mensaje_success = req.session.success_msg;

    req.session.success_msg = null;
    req.session.error_msg = null;

    res.render('backend/usuarios/form',{
        msg_success          : mensaje_success,
        msg_error            : mensaje_error
    });

});


router.get('/enable_disable/:id', function( req, res, next ) {
    var endpoint = go_config.endpoint + '/usuarios/enable_disabled';
    fetch( endpoint, {
        method: 'POST',
        headers: go_config.fetch_headers_default,
        body: JSON.stringify({id: req.params['id']}),
    } )
        .then( res => res.json() )
        .then( json => {
            req.session.success_msg = 'Usuario actualizado correctamente';
            setTimeout(function(){
                res.redirect('/usuarios/');
            },1000);

        }).catch((err) => {
            req.session.error_msg = 'Ha ocurrido un problema al actualizar el usuario';
            res.redirect('/usuarios');
        });
});

router.get('/delete/:id', function( req, res, next ) {
    var endpoint = go_config.endpoint + '/usuarios/delete';
    

    fetch( endpoint, {
        method: 'POST',
        headers: go_config.fetch_headers_default,
        body: JSON.stringify({id: req.params['id']}),
    } )
        .then( res => res.json() )
        .then( json => {
            req.session.success_msg = 'Usuario eliminado correctamente';
            setTimeout(function(){
                res.redirect('/usuarios/');
            },1000);

        }).catch((err) => {
            req.session.error_msg = 'Ha ocurrido un problema al eliminar el usuario';
            res.redirect('/usuarios');
        });
});

router.post('/save', function( req, res, next ) {

    

    var endpoint = go_config.endpoint + '/usuarios/save';
    var tiene_id = req.body.id;

    fetch( endpoint, {
        method: 'POST',
        headers: go_config.fetch_headers_default,
        body: JSON.stringify(req.body),
    } )
        .then( res => res.json() )
        .then( json => {
            
            if( json.success ) {
                if( tiene_id > 0 ) {
                    req.session.success_msg = 'El usuario ha sido editado correctamente';
                    res.redirect('/usuarios/edit/' + json.id);
                } else {
                    req.session.success_msg = 'El usuario ha sido creado correctamente';
                    res.redirect('/usuarios/edit/' + json.id);
                }
            } else {
                if( tiene_id > 0 ) {
                    req.session.error_msg = json.message;
                    res.redirect('/usuarios/edit/' + json.id + '?error');
                    
                } else {
                    req.session.error_msg = json.message;
                    res.redirect('/usuarios/create/');
                }
            }


        }).catch((err) => {
            console.log('error' + err)
        });
});

module.exports = router;
