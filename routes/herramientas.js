var express = require('express');
var router = express.Router();
var fetch = require('node-fetch');
var go_config = require('../bin/config');

/* GET home page. */
router.get('/', function(req, res, next) {

    var endpoint = go_config.endpoint + '/herramientas/get_all';

    var mensaje_error = req.session.error_msg;
    var mensaje_success = req.session.success_msg;

    req.session.success_msg = null;
    req.session.error_msg = null;

    fetch( endpoint,{
        method: 'POST',
        headers: go_config.fetch_headers_default,
    } )
        .then( res => res.json() )
        .then( json => {
            
            if( json.success ) {
                res.render('backend/herramientas/index', { 
                    data: json.data,
                    msg_success : mensaje_success,
                    msg_error   : mensaje_error
                });
            } else {
                res.render('backend/herramientas/error');
                
            }

        } )
        .catch( err => {
            res.render('backend/herramientas/error');
        } )

});


router.get('/edit/:id', function( req, res, next ) {
    var endpoint = go_config.endpoint + '/herramientas/get_by_epc';
    var id = req.params['id'];
    var mensaje_error = req.session.error_msg;
    var mensaje_success = req.session.success_msg;

    req.session.success_msg = null;
    req.session.error_msg = null;

    fetch( endpoint, {
        method: 'POST',
        headers: go_config.fetch_headers_default,
        body: JSON.stringify({epc: id}),
    } )
        .then( res => res.json() )
        .then( json => {
            res.render('backend/herramientas/form', {
                epc              : json.data.epc,
                nombre           : json.data.nombre,
                img              : json.data.img,
                idTipo           : json.data.idTipo,
                detalle          : json.data.detalle,
                id               : json.data.id,
                fechaVencimiento : json.data.fechaVencimiento,
                msg_success      : mensaje_success,
                msg_error        : mensaje_error
            })
        }).catch((err) => {
            res.render('backend/herramientas/error')
        });


});

router.get('/create', function( req, res, next ){

    var mensaje_error = req.session.error_msg;
    var mensaje_success = req.session.success_msg;

    req.session.success_msg = null;
    req.session.error_msg = null;

    res.render('backend/herramientas/form',{
        msg_success          : mensaje_success,
        msg_error            : mensaje_error
    });

});

router.post('/ajax/tipo_herramientas', function( req, res, next ) {
    var endpoint = go_config.endpoint + '/herramientatipo/get_all';
    fetch( endpoint, {
        method: 'POST',
        headers: go_config.fetch_headers_default,
    } )
        .then( res => res.json() )
        .then( json => {
            
            res.send({'success': true, 'response': json.data});
        }).catch((err) => {
            res.send({'success': false, 'response': [] });
        });
});

router.post('/ajax/epc_available', function( req, res, next ) {
    var endpoint = go_config.endpoint + '/epcs/get_available';
    fetch( endpoint, {
        method: 'POST',
        headers: go_config.fetch_headers_default,
    } )
        .then( res => res.json() )
        .then( json => {
            res.send({'success': true, 'response': json.data});
        }).catch((err) => {
            res.send({'success': false, 'response': [] });
        });
});

router.get('/delete/:epc', function( req, res, next ) {
    var endpoint = go_config.endpoint + '/herramientas/delete';
    
    fetch( endpoint, {
        method: 'POST',
        headers: go_config.fetch_headers_default,
        body: JSON.stringify({epc: req.params['epc']}),
    } )
        .then( res => res.json() )
        .then( json => {
            req.session.success_msg = 'Herramienta eliminada correctamente';

            setTimeout(function(){
                res.redirect('/herramientas');
            }, 1000);


        }).catch((err) => {
            req.session.error_msg = 'Ha ocurrido un problema eliminado la herramienta';
            res.redirect('/herramientas');
        });
});

router.post('/save', function( req, res, next ) {
    var endpoint = go_config.endpoint + '/herramientas/save';
    var tiene_id = req.body.id;

    fetch( endpoint, {
        method: 'POST',
        headers: go_config.fetch_headers_default,
        body: JSON.stringify(req.body),
    } )
        .then( res => res.json() )
        .then( json => {
            
            if( json.success ) {
                if( tiene_id > 0 ) {
                    req.session.success_msg = 'La herramienta ha sido editada correctamente';
                    setTimeout(function(){
                        res.redirect('/herramientas/edit/' + json.epc);
                    },1000);
                } else {
                    req.session.success_msg = 'La herramienta ha sido creada correctamente';
                    setTimeout(function(){
                        res.redirect('/herramientas/edit/' + json.epc);
                    },1000);
                }
            } else {
                if( tiene_id > 0 ) {
                    req.session.error_msg = json.message;
                    setTimeout(function(){
                        res.redirect('/herramientas/edit/' + json.epc);
                    },1000);
                    
                } else {
                    req.session.error_msg = json.message;
                    setTimeout(function(){
                        res.redirect('/herramientas/create/');
                    },1000);
                }
            }

        }).catch((err) => {
            console.log('error' + err)
        });
});

module.exports = router;
