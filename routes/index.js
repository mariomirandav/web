var express = require('express');
var router = express.Router();
var fetch = require('node-fetch');
var go_config = require('../bin/config');
var excel = require('excel4node');
var fs = require('fs');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render("backend/hogar/index", {title:"Dashboard"});
});

router.get('/informe', function(req, res, next) {
  res.render("backend/hogar/informe", {title:"Dashboard"});
});



router.post('/ajax/reporte', function(req, res, next) {
  var endpoint = go_config.endpoint + '/information/report';


    fetch( endpoint, {
        method: 'POST',
        headers: go_config.fetch_headers_default,
        body: JSON.stringify({
          date_start: req.body.date_start,
          date_end: req.body.date_end,
          type: req.body.type
        }),
        
    } )
        .then( res => res.json() )
        .then( json => {

          var workbook = new excel.Workbook();
          var worksheet = workbook.addWorksheet('Movimiento herramientas');

          console.log(json.success)

          if( json.success ) {
            let index = 1;
            let titulos = [];
            for( var row in json.data ) {
              titulos = Object.keys( json.data[row] );
              break;
            }

            var i = 1;
            for( var titulo in titulos ) {
              worksheet.cell(1,i).string( titulos[titulo] );
              i++;
            }
            var u = 2;
            for( var row in json.data ) {
              
              var v = 1;
              for( var titulo_nodo in titulos ) {
                var _nodo = titulos[titulo_nodo];
                var valor = json.data[ row ][ _nodo ];
              
                worksheet.cell(u, v).string( String(valor) );
                v++;

              }

              u++;
            }


          }

          workbook.write('public/download/report/report.xlsx');
            
            res.send({'success': true,'file' : '/public/download/report/report.xlsx', 'response': json.data});
        }).catch((err) => {
          console.log(err)
            res.send({'success': false, 'response': [] });
        });
});

router.post('/ajax/dashboard', function(req, res, next){
  var endpoint =  go_config.endpoint + '/information/dashboard';

  fetch( endpoint, {
    method: 'POST',
    headers: go_config.fetch_headers_default,
  } )
      .then( res => res.json() )
      .then( json => {
          if( ! json.status ) {
              res.send({'success':false});
              return;
          }

          res.send( json )

      } );
});

module.exports = router;
