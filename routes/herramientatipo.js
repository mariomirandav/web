var express = require('express');
var router = express.Router();
var fetch = require('node-fetch');
var go_config = require('../bin/config');

/* GET home page. */
router.get('/', function(req, res, next) {

    var endpoint = go_config.endpoint + '/herramientatipo/get_all';

    var mensaje_error = req.session.error_msg;
    var mensaje_success = req.session.success_msg;

    req.session.success_msg = null;
    req.session.error_msg = null;

    fetch( endpoint,{
        method: 'POST',
        headers: go_config.fetch_headers_default,
    } )
        .then( res => res.json() )
        .then( json => {
            
            if( json.success ) {
                res.render('backend/herramientatipo/index', { 
                    data: json.data,
                    msg_success : mensaje_success,
                    msg_error   : mensaje_error
                });
            } else {
                res.render('backend/herramientatipo/error');
                
            }

        } )
        .catch( err => {
            res.render('backend/herramientatipo/error');
        } )

});


router.get('/edit/:id', function( req, res, next ) {
    var endpoint = go_config.endpoint + '/herramientatipo/get_by_id';
    var id = req.params['id'];
    var mensaje_error = req.session.error_msg;
    var mensaje_success = req.session.success_msg;

    req.session.success_msg = null;
    req.session.error_msg = null;

    fetch( endpoint, {
        method: 'POST',
        headers: go_config.fetch_headers_default,
        body: JSON.stringify({id: id}),
    } )
        .then( res => res.json() )
        .then( json => {
            res.render('backend/herramientatipo/form', {
                tipo        : json.data.tipo,
                id          : json.data.id,
                msg_success : mensaje_success,
                msg_error   : mensaje_error
            })
        }).catch((err) => {
            res.render('backend/herramientatipo/error');
        });


});

router.get('/create', function( req, res, next ){

    var mensaje_error   = req.session.error_msg;
    var mensaje_success = req.session.success_msg;

    req.session.success_msg = null;
    req.session.error_msg   = null;

    res.render('backend/herramientatipo/form',{
        msg_success : mensaje_success,
        msg_error   : mensaje_error
    });

});

router.get('/delete/:id', function( req, res, next ) {
    var endpoint = go_config.endpoint + '/herramientatipo/delete';
    
    fetch( endpoint, {
        method: 'POST',
        headers: go_config.fetch_headers_default,
        body: JSON.stringify({id: req.params['id']}),
    } )
        .then( res => res.json() )
        .then( json => {
            
            if( json.success ) {
                
                req.session.success_msg = 'Herramienta tipo eliminado correctamente';
                setTimeout(function(){
                    res.redirect('/herramientatipo');
                }, 1000);
            } else {
                req.session.error_msg = json.message;
                setTimeout(function(){
                    res.redirect('/herramientatipo');
                }, 1000);
            }



        }).catch((err) => {
            req.session.error_msg = 'Ha ocurrido un problema eliminado la herramienta tipo';
            res.redirect('/herramientatipo');
        });
});

router.post('/save', function( req, res, next ) {
    var endpoint = go_config.endpoint + '/herramientatipo/save';
    var tiene_id = req.body.id;

    fetch( endpoint, {
        method: 'POST',
        headers: go_config.fetch_headers_default,
        body: JSON.stringify(req.body),
    } )
        .then( res => res.json() )
        .then( json => {

            
            if( json.success ) {
                if( tiene_id > 0 ) {
                    req.session.success_msg = 'La herramienta tipo ha sido editada correctamente';
                    
                    setTimeout(function(){
                        res.redirect('/herramientatipo/edit/' + json.id);
                    }, 1000)
                } else {
                    req.session.success_msg = 'La herramienta tipo ha sido creada correctamente';
                    setTimeout(function(){
                        res.redirect('/herramientatipo/edit/' + json.id);

                    },1000);
                }
            } else {
                if( tiene_id > 0 ) {
                    req.session.error_msg = json.message;
                    res.redirect('/herramientatipo/edit/' + json.id + '?error');
                    
                } else {
                    req.session.error_msg = json.message;
                    res.redirect('/herramientatipo/create/');
                }
            }

        }).catch((err) => {
            console.log('error' + err)
        });
});

module.exports = router;
