var express = require('express');
var router = express.Router();
var fetch = require('node-fetch');
var go_config = require('../bin/config');

/* GET home page. */
router.get('/', function(req, res, next) {

    var endpoint = go_config.endpoint + '/epcs/get_all';

    var mensaje_error = req.session.error_msg;
    var mensaje_success = req.session.success_msg;

    req.session.success_msg = null;
    req.session.error_msg = null;


    fetch( endpoint,{
        method: 'POST',
        headers: go_config.fetch_headers_default
    } )
        .then( res => res.json() )
        .then( json => {
            
            if( json.success ) {
                res.render('backend/epcs/index', { 
                    data: json.data,
                    msg_success : mensaje_success,
                    msg_error   : mensaje_error
                });
            } else {
                res.render('backend/epcs/error');
                
            }

        } )
        .catch( err => {
            res.render('backend/epcs/error');
        } )

});


router.get('/edit/:id', function( req, res, next ) {
    var endpoint = go_config.endpoint + '/epcs/get_by_id';
    var id = req.params['id'];
    var mensaje_error = req.session.error_msg;
    var mensaje_success = req.session.success_msg;

    req.session.success_msg = null;
    req.session.error_msg = null;


    fetch( endpoint, {
        method: 'POST',
        headers: go_config.fetch_headers_default,
        body: JSON.stringify({id: id}),
    } )
        .then( res => res.json() )
        .then( json => {
            res.render('backend/epcs/form', {
                epc         : json.data.epc,
                idMark          : json.data.idMark,
                id: json.data.id,
                msg_success : mensaje_success,
                msg_error   : mensaje_error
            })
        }).catch((err) => {
            res.render('backend/epcs/error')
        });


});

router.get('/create', function( req, res, next ){

    var mensaje_error = req.session.error_msg;
    var mensaje_success = req.session.success_msg;

    req.session.success_msg = null;
    req.session.error_msg = null;

    res.render('backend/epcs/form',{
        msg_success          : mensaje_success,
        msg_error            : mensaje_error
    });

});

router.post('/ajax/epc_available', function( req, res, next ) {
    var endpoint = go_config.endpoint + '/epcs/get_available';
    fetch( endpoint, {
        method: 'POST',
        headers: go_config.fetch_headers_default,
    } )
        .then( res => res.json() )
        .then( json => {
            console.log(json)
            res.send({'success': true, 'response': json.data});
        }).catch((err) => {
            res.send({'success': false, 'response': [] });
        });
});

router.get('/delete/:id', function( req, res, next ) {
    var endpoint = go_config.endpoint + '/epcs/delete';
    
    fetch( endpoint, {
        method: 'POST',
        headers: go_config.fetch_headers_default,
        body: JSON.stringify({id: req.params['id']}),
    } )
        .then( res => res.json() )
        .then( json => {
            
            if( json.success ) {
                
                req.session.success_msg = 'Epc eliminado correctamente';
                setTimeout(function(){
                    res.redirect('/epcs');
                }, 1000);
            } else {
                req.session.error_msg = json.message;
                setTimeout(function(){
                    res.redirect('/epcs');
                }, 1000);
            }



        }).catch((err) => {
            req.session.error_msg = 'Ha ocurrido un problema eliminado el epc';
            res.redirect('/epcs');
        });
});

router.post('/save', function( req, res, next ) {
    var endpoint = go_config.endpoint + '/epcs/save';
    var tiene_id = req.body.id;

    fetch( endpoint, {
        method: 'POST',
        headers: go_config.fetch_headers_default,
        body: JSON.stringify(req.body),
    } )
        .then( res => res.json() )
        .then( json => {

            
            if( json.success ) {
                if( tiene_id > 0 ) {
                    req.session.success_msg = 'La herramienta ha sido editada correctamente';
                    
                    setTimeout(function(){
                        res.redirect('/epcs/edit/' + json.id);
                    }, 1000)
                } else {
                    req.session.success_msg = 'La herramienta ha sido creada correctamente';
                    setTimeout(function(){
                        res.redirect('/epcs/edit/' + json.id);

                    },1000);
                }
            } else {
                if( tiene_id > 0 ) {
                    console.log('xxx');
                    req.session.error_msg = json.message;
                    res.redirect('/epcs/edit/' + tiene_id + '?error');
                    
                } else {
                    console.log('uuu');
                    req.session.error_msg = json.message;
                    res.redirect('/epcs/create/');
                }
            }

        }).catch((err) => {
            console.log('error' + err)
        });
});

module.exports = router;
